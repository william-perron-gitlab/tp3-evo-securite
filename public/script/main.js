$(".activator").click(function() {
    let id = $(this).attr("data-activate")
    let $elem =$("#" + id);
    $elem.toggleClass("hidden");

    let tabindex = $elem.attr("tabindex");
    if(tabindex=="-1"){
        $elem.attr("tabindex", "1");
        $elem.focus();      
    }else{
        $elem.attr("tabindex", "-1");
        $elem.focusout();
    }   
});


//  On sélectionne tous les élément qui peuvent avoir le focus dans ma modale
let $tab_elements = $("#modale-reservation").find("input, a, textarea, select, button");

//  On place dans des variables le premier noeud et le derniers noeud de cette sélection

let $first_tab_element = $tab_elements.first(); // Le bouton de fermeture de la modale (X)
let $last_tab_element = $tab_elements.last(); // Le bouton "Réserver"

// On ajoute un événement .keydown() (touche de calvier enfoncée) sur le bouton réserver 
// Si il s'agit de la touche "Tab" (identifier par le code 9 du clavier),
// On retire le focus du bouton réserver, pour l'appliquer au bouton de fermeture (X) 

$last_tab_element.keydown(function(event) {
    if ( event.which == 9 ) {
        event.preventDefault();
        $last_tab_element.focusout();
        $first_tab_element.focus();
    }
}); 

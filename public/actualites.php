<?php 
include_once('include/includes.php'); 
?>

  <!-- MAIN -->
  <main id="content">

    <!-- SECTION Actualités -->
    <div class="container">

      <h1>Actualités</h1>
      <div class="pad-x-1-asynch">
        <a href="#" class="bouton bouton-mauve pt-btn mot-clef">Toutes les actualités</a>
        <a href="#" class="bouton bouton-ghost pt-btn mot-clef">Critiques de livre</a>
        <a href="#" class="bouton bouton-ghost pt-btn mot-clef">Nouvelles</a>
        <a href="#" class="bouton bouton-ghost pt-btn mot-clef">Événement</a>
      </div>

      <div class="flex row">

        <div id="actualites" class="col-lg-8">

          <article class="actu-card clearfix">
            <div class="actu-img img-container fl">
              <img src="img/actu/actu01-batiment-gd.jpg">
            </div>
            <div class="actu-contenu fl">
              <span class="date">19 novembre 2018</span>
              <h3 class="no-margin">Des rénovations majeures durant l'hiver 2019</h3>
              <span>Par : <a href="#" class="auteur">Lisa Mc Kenzie</a></span>
              <p>Lorem ipsum dolor sit amet, mel no option quaerendum, quando malorum et eum. Vidisse minimum
                inimicus
                usu et. Eros cetero voluptaria cu vis, oblique eruditi maiestatis vis ex. Te vim saperet deseruisse.
                An
                pri imperdiet quaerendum.</p>
              <a href="#" class="lireSuite">Lire la suite <i class="fas fa-plus-circle"></i></a>
            </div>
          </article>

          <article class="actu-card clearfix">
            <div class="actu-img img-container fl">
              <img src="img/actu/actu02-fille-gd.jpg">
            </div>
            <div class="actu-contenu fl">
              <span class="date">10 novembre 2018</span>
              <h3 class="no-margin">Le calendrier de la période des fêtes</h3>
              <span>Par : <a href="#" class="auteur">Lisa Mc Kenzie</a></span>
              <p>Lorem ipsum dolor sit amet, mel no option quaerendum, quando malorum et eum. Vidisse minimum
                inimicus usu et. Eros cetero voluptaria cu vis, oblique eruditi maiestatis vis ex. Te vim saperet
                deseruisse. An pri imperdiet quaerendum.</p>
              <a href="#" class="lireSuite">Lire la suite <i class="fas fa-plus-circle"></i></a>
            </div>
          </article>

          <article class="actu-card clearfix">
            <div class="actu-img img-container fl">
              <img src="img/actu/actu03-rayons-gd.jpg">
            </div>
            <div class="actu-contenu fl">
              <span class="date">2 novembre 2018</span>
              <h3 class="no-margin">Les nouveautés à surveiller</h3>
              <span>Par : <a href="#" class="auteur">Lisa Mc Kenzie</a></span>
              <p>Lorem ipsum dolor sit amet, mel no option quaerendum, quando malorum et eum. Vidisse minimum
                inimicus usu et. Eros cetero voluptaria cu vis, oblique eruditi maiestatis vis ex. Te vim saperet
                deseruisse. An pri imperdiet quaerendum.</p>
              <a href="#" class="lireSuite">Lire la suite <i class="fas fa-plus-circle"></i></a>
            </div>
          </article>

          <article class="actu-card clearfix">
            <div class="actu-img img-container fl">
              <img src="img/actu/actu04-escaliers-gd.jpg" alt="Les rayonnages de la bibliothèque">
            </div>
            <div class="actu-contenu fl">
              <span class="date">26 octobre 2018</span>
              <h3 class="no-margin">De nouveaux escaliers pour la bibliothèque</h3>
              <span>Par : <a href="#" class="auteur">Lisa Mc Kenzie</a></span>
              <p>Lorem ipsum dolor sit amet, mel no option quaerendum, quando malorum et eum. Vidisse minimum
                inimicus usu et. Eros cetero voluptaria cu vis, oblique eruditi maiestatis vis ex. Te vim saperet
                deseruisse. An pri imperdiet quaerendum.</p>
              <a href="#" class="lireSuite">Lire la suite <i class="fas fa-plus-circle"></i></a>
            </div>
          </article>

          <article class="actu-card clearfix">
            <div class="actu-img img-container fl">
              <img src="img/actu/actu05-chars-meurent-gd.jpg" alt="Les rayonnages de la bibliothèque">
            </div>
            <div class="actu-contenu fl">
              <span class="date">17 octobre 2018</span>
              <h3 class="no-margin">Un auteur et son livre</h3>
              <span>Par : <a href="#" class="auteur">Lisa Mc Kenzie</a></span>
              <p>Lorem ipsum dolor sit amet, mel no option quaerendum, quando malorum et eum. Vidisse minimum
                inimicus usu et. Eros cetero voluptaria cu vis, oblique eruditi maiestatis vis ex. Te vim saperet
                deseruisse. An pri imperdiet quaerendum.</p>
              <a href="#" class="lireSuite">Lire la suite <i class="fas fa-plus-circle"></i></a>
            </div>
          </article>

          <!-- flex-pagination -->
            <div class="pagination-custom">
                <a href="#"><i class="fas fa-angle-left"></i></a>
                <a href="#" class="bouton numeroPage bouton-mauve">1</a>
                <a href="#" class="bouton numeroPage bouton-ghost">2</a>
                <a href="#" class="bouton numeroPage bouton-ghost">3</a>
                <a href="#" class="bouton numeroPage bouton-ghost">4</a>
                <a href="#" class="bouton numeroPage bouton-ghost">5</a>
                <a href="#" class="bouton numeroPage bouton-ghost">6</a>
                <a href="#"><i class="fas fa-angle-right"></i></a>
            </div>
          <!-- fin du flex-pagination -->

        </div> <!-- fin div#actualites -->

        <div id="liensActualites" class="col-lg-4">

          <h3 class="no-margin">Critiques de livre</h3>
          <nav class="pad-x-1-asynch">
            <ul>
              <li>
                <a href="#" class="bleu">Moi, ce que j'aime, c'est les monstres - Emil Ferris</a>
              </li>
              <li>
                <a href="#" class="bleu">L'amitié prodigieuse - Elena Ferrante</a>
              </li>
              <li>
                <a href="#" class="bleu">Cadillac - Biz</a>
              </li>
              <li>
                <a href="#" class="bleu">Le guide du mauvais père T.4 - Guy Délisle</a>
              </li>
              <li>
                <a href="#" class="bleu">Vingt-trois secrets bien gardés - Michel Tremblay</a>
              </li>
            </ul>
          </nav>

          <h3 class="no-margin">Les plus populaires</h3>
          <nav class="pad-x-1-asynch">
            <ul>
              <li>
                <a href="#" class="bleu">Des rénovations majeures durant l'hiver 2019</a>
              </li>
              <li>
                <a href="#" class="bleu">Le calendrier de la période des fêtes</a>
              </li>
              <li>
                <a href="#" class="bleu">Les nouveautés à surveiller</a>
              </li>
              <li>
                <a href="#" class="bleu">De nouveaux escaliers pour la bibliothèque</a>
              </li>
              <li>
                <a href="#" class="bleu">Un auteur et son livre</a>
              </li>
            </ul>
          </nav>

          <h3 class="no-margin">Les plus récents</h3>
          <nav class="pad-x-1-asynch">
            <ul>
              <li>
                <a href="#" class="bleu">Des rénovations majeures durant l'hiver 2019</a>
              </li>
              <li>
                <a href="#" class="bleu">Le calendrier de la période des fêtes</a>
              </li>
              <li>
                <a href="#" class="bleu">Les nouveautés à surveiller</a>
              </li>
              <li>
                <a href="#" class="bleu">De nouveaux escaliers pour la bibliothèque</a>
              </li>
              <li>
                <a href="#" class="bleu">Un auteur et son livre</a>
              </li>
            </ul>
          </nav>

          <h3 class="no-margin">Suivez-nous</h3>
          <nav class="soc-med" role="navigation">
            <ul class="inline-block">
              <li>
                <a href="https://www.facebook.com" target="_blank">
                  <i class="fab fa-facebook mauve"></i>
                </a>
              </li>
              <li>
                <a href="https://twitter.com" target="_blank">
                  <i class="fab fa-twitter-square mauve"></i>
                </a>
              </li>
              <li>
                <a href="https://www.youtube.com" target="_blank">
                  <i class="fab fa-youtube-square mauve"></i>
                </a>
              </li>
            </ul>
          </nav>

          <div id="infolettre-aside">
              <h3 id="infolettreTitle">Infolettre</h3>
              <p id="infolettreDesc">Pour rester informé des activités, nouveautés et actualités, abonnez-vous à notre
                infolettre :</p>
              <form action="#" method="POST" aria-label="Formulaire pour laisser son e-mail et recevoir l'infolettre de la bibliothèque">
                <!-- ??? <label for="infolettre">Courriel :</label> -->
                <div class="flex input-flex">
                  <input type="email" id="champInfolettre" name="champInfolettre" placeholder="exemple@mail.com"
                    aria-labelledby="infolettre" aria-describedby="infolettreDesc">
                  <input type="submit" class="bouton bouton-ghost" value="Envoyer">
                </div>
              </form>    
            </div>

          </div><!-- fin div#liensActualites flex-row -->

      </div>
      <!-- fin du flex-row -->
    </div>
    <!-- fin du container -->

  </main>
  <!-- fin du Main -->

<?php 
include_once('include/components/footer.php'); 
?>
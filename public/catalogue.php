<?php
include_once('include/includes.php');
$livres = getAllLivres();
?>

<!-- MAIN -->
<main id="content">

    <!-- SECTION Catalogue -->
    <section id="catalogue" class="container">
        <div class="catalogue">
            <div class="flex header-flex flex-wrap">
                <h1>Catalogue</h1>
                <div class="barre-recherche flex flex-wrap">
                    <a href="#" class="lireSuite">Recherche avancée <i class="fas fa-plus-circle"></i></a>
                    <form action="#" method="get">
                        <div class="flex input-flex">
                            <input type="text" id="recherche" name="recherche" placeholder="Titre, auteur, éditeur, catégorie, collection, etc.">
                            <button class="bouton bouton-recherche"><i class="fas fa-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
            <?php if (isset($livres)) : ?>
                <div class="row catalogue-flex">
                    <?php foreach ($livres as $livre) : ?>
                        <div class="livre-card col-6 col-md-4 col-lg-3">
                            <div class="livre-img img-container">

                                <a href="ficheLivre?id_livre=<?= $livre['id']; ?>">
                                    <?php
                                    $img = "img/livres/cover" . $livre['id'] . ".jpg";
                                    if (file_exists($img)) : ?>
                                       <img src="<?= $img; ?>">
                                    <?php else:?>
                                        <img src="img/livres/cover.jpg">
                                    <?php endif;?>
                                </a>
                            </div>
                            <div class="livre-infos">
                                <a href="ficheLivre?id_livre=<?= $livre['id']; ?>">
                                    <h4 class="ellipse"><?= $livre['titre'] ?></h4>
                                </a>
                                <p>Par : <a href="#" class="auteur">
                                        <span class="text-uppercase"><?= $livre['nomAuteur'] . '</span>,' . $livre['prenomAuteur']; ?>
                                    </a></p>
                                <a href="#" data-activate="modale-reservation" class="bouton bouton-bleu pt-btn activator">Réserver</a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>

        </div>
        <!-- fin de la div catalogue (= bordure du bas) -->
        <div class="pagination-custom">
            <a href="#"><i class="fas fa-angle-left"></i></a>
            <a href="#" class="bouton numeroPage bouton-mauve">1</a>
            <a href="#" class="bouton numeroPage bouton-ghost">2</a>
            <a href="#" class="bouton numeroPage bouton-ghost">3</a>
            <a href="#" class="bouton numeroPage bouton-ghost">4</a>
            <a href="#" class="bouton numeroPage bouton-ghost">5</a>
            <a href="#" class="bouton numeroPage bouton-ghost">6</a>
            <a href="#"><i class="fas fa-angle-right"></i></a>
        </div>
        <!-- fin du flex -->
    </section>
    <!-- fin du container -->

</main>
<!-- fin du Main -->

<!-- FENÊTRE MODALE -->
<div id="modale-reservation" tabindex="-1" class="modale-container hidden">
    <div data-activate="modale-reservation" class="overlay activator"></div>
    <div class="fenetre-modale">
        <h4>Réservation</h4>
        <a tabindex="1" data-activate="modale-reservation" class="fermer activator"><i class="fas fa-times-circle"></i></a>
        <form action="#" method="POST">
            <div>
                <label>Votre nom:</label>
                <input tabindex="1" type="text" id="nom" name="nom">
            </div>

            <div>
                <label>Date de la réservation:</label>
                <input tabindex="1" type="date" id="date" name="date">
            </div>

            <div>
                <label>Commentaire (facultatif):</label>
                <textarea tabindex="1" rows="4" cols="50" id="commentaire" name="commentaire"></textarea>
            </div>

            <input tabindex="1" type="button" class="bouton-fenetre" value="Réserver">
        </form>
    </div>
</div> <!-- Fin FENÊTRE MODALE -->

<?php include_once('include/components/footer.php');  ?>
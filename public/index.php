<?php 
include_once('include/includes.php');
?>


<!-- section HERO-BANNER -->
<div id="hero-banner">
  <div class="container flex hero-flex">
    <div class="recherche-box">
      <form action="#" method="get">
        <label id="trouverUnLivre" class="bouton-recherche">Trouver un livre :</label>
        <div class="flex input-flex">
          <input class="search" type="text" name="recherche" placeholder="Titre, auteur, éditeur, catégorie, collection, etc.">
          <button class="bouton bouton-recherche"><i class="fas fa-search"></i></button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Contenu de la page -->
<main id="content">

  <!-- Contenu Accueil -->
  <div id="accueil" class="container">
    <div class="flex row">

      <section id="nouveautes-1" class="col-md-4">
        <h2 class="h2-as-h1">Nouveautés</h2>
        <div class="owl-carousel owl-theme">
          <a href="ficheLivre?id_livre=1">
            <img src="img/livres/cover1.jpg">
          </a>
          <a href="ficheLivre?id_livre=4">
            <img src="img/livres/cover4.jpg">
          </a>
          <a href="ficheLivre?id_livre=6">
            <img src="img/livres/cover6.jpg">
          </a>
        </div>
      </section>

      <section id="actualites-3" class="col-md-8">
        <h2 class="h2-as-h1">Actualités</h2>
        <div class="actu-card clearfix">
          <div class="actu-img img-container fl">
            <img src="img/actu/actu01-batiment-pt.jpg">
          </div>
          <div class="actu-contenu fl">
            <span class="date">19 novembre 2018</span>
            <h3>Des rénovations majeures durant l'hiver 2019</h3>
            <p>Lorem ipsum dolor sit amet, mel no option quaerendum, quando malorum et eum. Vidisse minimum inimicus
              usu et. Eros cetero voluptaria cu vis, oblique eruditi maiestatis vis ex. Te vim saperet deseruisse. An
              pri imperdiet quaerendum.</p>
            <a href="#" class="lireSuite">Lire la suite <i class="fas fa-plus-circle"></i></a>
          </div>
        </div>
        <div class="actu-card clearfix">
          <div class="actu-img img-container fl">
            <img src="img/actu/actu02-fille-pt.jpg">
          </div>
          <div class="actu-contenu fl">
            <span class="date">10 novembre 2018</span>
            <h3>Le calendrier de la période des fêtes</h3>
            <p>Lorem ipsum dolor sit amet, mel no option quaerendum, quando malorum et eum. Vidisse minimum
              inimicus usu et. Eros cetero voluptaria cu vis, oblique eruditi maiestatis vis ex. Te vim saperet
              deseruisse. An pri imperdiet quaerendum.</p>
            <a href="#" class="lireSuite">Lire la suite <i class="fas fa-plus-circle"></i></a>
          </div>
        </div>
        <div class="actu-card clearfix">
          <div class="actu-img img-container fl">
            <img src="img/actu/actu03-rayons-pt.jpg">
          </div>
          <div class="actu-contenu fl">
            <span class="date">2 novembre 2018</span>
            <h3>Les nouveautés à surveiller</h3>
            <p>Lorem ipsum dolor sit amet, mel no option quaerendum, quando malorum et eum. Vidisse minimum
              inimicus usu et. Eros cetero voluptaria cu vis, oblique eruditi maiestatis vis ex. Te vim saperet
              deseruisse. An pri imperdiet quaerendum.</p>
            <a href="#" class="lireSuite">Lire la suite <i class="fas fa-plus-circle"></i></a>
          </div>
        </div>

      </section>

      <section id="activites-2" class="col-12">
        <h2 class="h2-as-h1">Activités</h2>
        <div class="row">
          <a href="#" class="activite">
            <i class="fas fa-plus-circle"></i>
            <p>Toutes les activités</p>
          </a>
          <a href="#" class="activite">
            <i class="fas fa-smile-beam"></i>
            <p>Activités jeunesse</p>
          </a>
          <a href="#" class="activite">
            <i class="fas fa-pencil-ruler"></i>
            <p>Ateliers et animations</p>
          </a>
          <a href="#" class="activite">
            <i class="fas fa-film"></i>
            <p>Cinéma</p>
          </a>
          <a href="#" class="activite">
            <i class="fas fa-theater-masks"></i>
            <p>Spectacles</p>
          </a>
          <a href="#" class="activite">
            <i class="fas fa-comments"></i>
            <p>Conférences et rencontres</p>
          </a>
          <a href="#" class="activite">
            <i class="fas fa-palette"></i>
            <p>Expositions</p>
          </a>
          <a href="#" class="activite">
            <i class="fas fa-laptop"></i>
            <p>Technos</p>
          </a>
          <a href="#" class="activite">
            <i class="fas fa-trophy"></i>
            <p>Concours</p>
          </a>
        </div>
      </div>

    </section>
    <!-- fin du flex -->
  </div>
  <!-- fin du container -->
</main>

<?php include_once('include/components/footer.php');  ?>
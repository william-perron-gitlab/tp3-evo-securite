<?php
$root = "../";
include_once $root . 'include/includes.php';

$data = array();
$data['categorie'] = $data['genre'] = "";

if (!empty($_POST)) {

    /* Validation des champs du livre */
    include_once 'utils/validate-livre.php';

    if (empty($error)) {
        storeLivre($data);
        header("Location: index.php?message=success");
    }
}
?>
<main id="gestion-livres" class="container">
    <h3>Ajouter un livre</h3>
    <form action="livreCreate.php" method="POST" class="p-4 bg-light border">
        <?php if (!empty($error)) : ?>
            <div class="erreur alert alert-danger mt-2 d-inline-block" role="alert">
                Veuillez corriger les erreurs suivantes :
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="form-group">
                <label for="isbn">ISBN</label>
                <input id="isbn" class="form-control" name="isbn" type="number" value="<?php echo !empty($data['isbn']) ? $data['isbn'] : ''; ?>">
                <?php if (!empty($error['isbn'])) : ?>
                    <div>
                        <span class="erreur alert alert-danger mt-2 d-inline-block"><?php echo $error['isbn']; ?></span>
                    </div>
                <?php endif; ?>
            </div>

            <div class="form-group">
                <label for="titre">Titre</label>
                <input id="titre" class="form-control" name="titre" type="text" value="<?php echo !empty($data['titre']) ? $data['titre'] : ''; ?>">
                <?php if (!empty($error['titre'])) : ?>
                    <div>
                        <span class="erreur alert alert-danger mt-2 d-inline-block"><?php echo $error['titre']; ?></span>
                    </div>
                <?php endif; ?>
            </div>

            <div class="form-group">
                <label for="nomAuteur">Nom de l'auteur</label>
                <input id="nomAuteur" class="form-control" name="nomAuteur" type="text" value="<?php echo !empty($data['nomAuteur']) ? $data['nomAuteur'] : ''; ?>">
                <?php if (!empty($error['nomAuteur'])) : ?>
                    <div>
                        <span class="erreur alert alert-danger mt-2 d-inline-block"><?php echo $error['nomAuteur']; ?></span>
                    </div>
                <?php endif; ?>
            </div>

            <div class="form-group">
                <label for="prenomAuteur">Nom de l'auteur</label>
                <input id="prenomAuteur" class="form-control" name="prenomAuteur" type="text" value="<?php echo !empty($data['prenomAuteur']) ? $data['prenomAuteur'] : ''; ?>">
                <?php if (!empty($error['prenomAuteur'])) : ?>
                    <div>
                        <span class="erreur alert alert-danger mt-2 d-inline-block"><?php echo $error['prenomAuteur']; ?></span>
                    </div>
                <?php endif; ?>
            </div>

            <div class="form-group">
                <label for="categorie">Catégorie</label>
                <select name="categorie" id="categorie" class="form-control">
                    <option value=""></option>
                    <option value="1" <?php echo $data['categorie'] == "1" ? 'selected' : ''; ?>>Roman</option>
                    <option value="2" <?php echo $data['categorie'] == "2" ? 'selected' : ''; ?>>Référence</option>
                    <option value="3" <?php echo $data['categorie'] == "3" ? 'selected' : ''; ?>>Jeunesse</option>
                    <option value="4" <?php echo $data['categorie'] == "4" ? 'selected' : ''; ?>>Bande-dessinée</option>
                </select>
                <?php if (!empty($error['categorie'])) : ?>
                    <div>
                        <span class="erreur alert alert-danger mt-2 d-inline-block"><?php echo $error['categorie']; ?></span>
                    </div>
                <?php endif; ?>
            </div>

            <div class="form-group">
                <label for="genre">Genre</label>
                <select name="genre" id="genre" class="form-control">
                    <option value=""></option>
                    <option value="1" <?php echo $data['genre'] == "1" ? 'selected' : ''; ?>>Action</option>
                    <option value="2" <?php echo $data['genre'] == "2" ? 'selected' : ''; ?>>Humour</option>
                    <option value="3" <?php echo $data['genre'] == "3" ? 'selected' : ''; ?>>Drame</option>
                    <option value="4" <?php echo $data['genre'] == "4" ? 'selected' : ''; ?>>Histoire</option>
                </select>
                <?php if (!empty($error['genre'])) : ?>
                    <div>
                        <span class="erreur alert alert-danger mt-2 d-inline-block"><?php echo $error['genre']; ?></span>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <textarea name="description" id="description" class="form-control"><?php echo !empty($data['description']) ? $data['description'] : ''; ?></textarea>
            <?php if (!empty($error['description'])) : ?>
                <div>
                    <span class="erreur alert alert-danger mt-2 d-inline-block"><?php echo $error['description']; ?></span>
                </div>
            <?php endif; ?>
        </div>
        <div class="flex justify-content-end">
            <a href="index.php" class="btn btn-outline-secondary mr-2">Retour</a>
            <input class="btn btn-primary" type="submit" value="Soumettre">
        </div>

    </form>
</main>
<?php include_once  $root . 'include/components/footer.php'; ?>
var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefix  = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();
var rename = require('gulp-rename');

gulp.task('watch', function(){
    browserSync.init({
        proxy   : "http://localhost/EVO-TP3-H21/public/"
        // @TODO : corriger pour éviter d'avoir à utiliser proxy
        /*server: {
            baseDir: "./public",
            index: "/index.php"
        }*/
    })
    gulp.watch('scss/**/*.scss', gulp.series('sass')); 
    gulp.watch('public/**/*.php').on('change', browserSync.reload);
    gulp.watch('public/**/*.html').on('change', browserSync.reload);
    gulp.watch('public/**/*.js').on('change', browserSync.reload);
})

gulp.task('sass', function(){
    return gulp.src('scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}))
        .on('error', function(err){
            browserSync.notify(err.message, 20000);
            this.emit('end');
        })
        .pipe(autoprefix())
        .pipe(rename({ extname: '.min.css' }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('public/css'))
        .pipe(browserSync.reload({stream: true}));
});




